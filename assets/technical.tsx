<?xml version="1.0" encoding="UTF-8"?>
<tileset name="technical" tilewidth="96" tileheight="96">
 <image source="technical.gif" width="384" height="192"/>
 <tile id="0">
  <properties>
   <property name="components" value="Obstacle"/>
  </properties>
 </tile>
 <tile id="1">
  <properties>
   <property name="components" value="start"/>
  </properties>
 </tile>
 <tile id="2">
  <properties>
   <property name="components" value="Win"/>
  </properties>
 </tile>
 <tile id="3">
  <properties>
   <property name="components" value="Obstacle,Instakill"/>
  </properties>
 </tile>
 <tile id="4">
  <properties>
   <property name="components" value="TopTrap"/>
  </properties>
 </tile>
 <tile id="5">
  <properties>
   <property name="components" value="LeftTrap"/>
  </properties>
 </tile>
 <tile id="6">
  <properties>
   <property name="components" value="RightTrap"/>
  </properties>
 </tile>
 <tile id="7">
  <properties>
   <property name="components" value="BotTrap"/>
  </properties>
 </tile>
</tileset>
