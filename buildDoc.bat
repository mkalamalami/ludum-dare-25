@echo off
jsdoc components/*.js game/*.js -d docs/

REM To install jsdoc, make sure you have node and run:
REM npm install -g git://github.com/jsdoc3/jsdoc.git
REM (you might need to add it to your PATH: C:\Users\USERNAME\AppData\Roaming\npm\node_modules\jsdoc)