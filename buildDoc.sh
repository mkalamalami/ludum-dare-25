#!/bin/sh

jsdoc components/*.js game/*.js -d docs/

# To install jsdoc, make sure you have node and run:
# npm install -g git://github.com/jsdoc3/jsdoc.git