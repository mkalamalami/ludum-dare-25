define(function() {

/**
    <p>Makes an entity bounce vertically.</p>
        
    @class
    @name Bounce
    
    @example
    Crafty.e('2D, DOM, Bounce').randomBoolean();
*/
Crafty.c('Bounce', { 

  init: function() {
    this.requires('2D');
    this.bind('EnterFrame', this._updateBounce);
  },
  
  /**
    @memberof Bounce#
    @param {integer} amplitude - The bounce amplitude in pixels (default: 5)
    @param {integer} frequency - The bounce frequency in periods per second (default: 1)
    @param {integer} offset - An offset in frames (default: 0)
  */
  bouncey: function(amplitude, frequency, offset) {
    this.amplitude = (amplitude !== undefined) ? amplitude : 5;
    this.frequency = (frequency !== undefined) ? frequency : 1;
    this.offset = (offset !== undefined) ? offset : 0;
    this.baseY = this.y;
    this.bouncingPaused = false;
    this._fps = Crafty.timer.FPS();
    return this;
  },
  
  /**
    @memberof Bounce#
    @param {boolean} pause - Allows to set the pause. Pause is toggled if undefined.
  */
  pauseBouncing: function(pause) {
    this.bouncingPaused = (typeof pause == 'boolean') ? pause : !this.bouncingPaused;
  },
  
  _updateBounce: function(e) {
    if (this.baseY && !this.bouncingPaused) {
      this.attr({
        y: this.baseY + this.amplitude*Math.cos(this.offset + e.frame*this.frequency/this._fps)
      });
    }
  }
  
});

});