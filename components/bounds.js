define(function() {

/**
    <p>Functions to help restrict values to min/max values.</p>
        
    @class
    @name Bounds

    @example
    var e = c.e('Bounds').setBound('x', [0, 100]);
    e.x = e.checkBounds('x', 101);
    console.log(e.x); // 100
*/
Crafty.c('Bounds', {

    init: function() {
        this._bounds = {};
    },
    
    /**
        @memberof Bounds#
        @param {string} property - a key
        @param {array} values - a [min,max] array
        @return this
     */
    setBounds: function(property, values) {
        this._bounds[property] = values;
        return this;
    },
    
    /**
        @memberof Bounds#
        @param {string} property - a key
        @param {integer} value - the value
        @return the value or min or max
     */
    checkBounds: function(property, value) {
        var propBounds = this._bounds[property];
        if (propBounds) {
            if (value < propBounds[0]) {
                value = propBounds[0];
            }
            if (value > propBounds[1]) {
                value = propBounds[1];
            }
        }
        return value;
    }

});

});