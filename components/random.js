define(function() {

/**
    <p>Some utilities for random numbers.</p>
        
    @class
    @name Random
    
    @example
    Crafty.e('Random').randomBoolean();
*/
Crafty.c('Random', {

    /**
    Returns a random number between `0` and `i`.
    @memberof Random#
    @param {interger} i
    */
    random: function(i) {
        return Math.floor(Math.random()*i);
    },
    
    /**
    Returns randomly `true` or `false`.
    @memberof Random#
    */
    randomBoolean: function() {
        return (Math.random() > 0.5);
    },
    
    /**
    Returns a random element from the given array.
    @memberof Random#
    @param {array} array
    */
    randomElement: function(array) {
        return array[Utils.random(array.length)];
    }

});

});