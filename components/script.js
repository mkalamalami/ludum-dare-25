define(function() {

/**
    <p>Allows to schedule calls to any object (time unit = frames)</p>
        
    @class
    @name Script
    
    @example
    var square = Crafty.e('2D, DOM').attr({w: 50, h: 50}).css({'background-color': 'black'});
    square.fall = function(height) {
        this.y += height;
    };
    
    Crafty.e('Script')
        .action(10, square, function(){ this.x += 10 })
        .action(10, square, 'fall', 10)
        .run(true);
*/
Crafty.c('Script', {
  init: function() {
	this.requires('2D, Keyboard');
    this.t = 0;
    this.actions = [];
    this.bind('EnterFrame', this._scriptEnterFrame);
    this.pause = true;
    this.loop = true;
    this.currentActionIndex = 0;
    this.currentAction = null;
  },
  
  /**
    @memberof Script#
    @param {integer} delay
    @param {object} context
    @param {function or string} method
    @param {...} arguments Optional
  */
  action: function(delay, context, method /* [args] */) {
    var args = [];
    if (arguments.length > 3) {
      for (var i = 3; i < arguments.length; i++) {
        args.push(arguments[i]);
      }
    }
    this.actions.push({
      delay: delay,
      context: context,
      method: (typeof method == 'function') ? method : context[method],
      arguments: args
    });
    return this;
  },
  
  /**
    @memberof Script#
    @param {boolean} loop
  */
  run: function(loop, skippable) {
    this.pause = false;
    this.loop = (loop !== undefined) ? loop : false;
    this.skippable = skippable || false;
    return this;
  },
  
  /**
    @memberof Script#
    @param {boolean} togglePause
  */
  togglePause: function() {
    this.pause = !this.pause;
    return this;
  },
  
  _scriptEnterFrame: function() {
    if (!this.pause) {
      if (this.currentActionIndex >= this.actions.length) {
        if (this.loop) {
          this.currentActionIndex = 0;
        }
        else {
          this.pause = true;
          return;
        }
      }
      if (this.currentAction == null) {
        this.currentAction = this.actions[this.currentActionIndex];
      }
      
      this.t += 1000. / c.timer.FPS();
      
      if (this.skippable && this.isDown('ENTER')) {
        this.t = this.currentAction.delay;
        c('Subtitle').destroy();
        c('Skip').destroy();
      }
    
      if (this.currentAction.delay <= this.t) {
        this.currentAction.method.apply(this.currentAction.context, this.currentAction.arguments);
        this.currentAction = null;
        this.currentActionIndex++;
        this.t = 0;
      }
    }
  }
});

});