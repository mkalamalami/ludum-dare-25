define(function() {

/**
    @class
    @name TiledMap
*/
Crafty.c('TiledMap', {

    init: function() {
        this._tileSetInfo = []; /* { name, min, max, lineWidth, spriteSize } */
        this._tileProperties = {}; /* Stores properties by tile ID */
    },

    /**
        <p>Parses maps from Tiled files.</p>
    
        <p>The parser supports:<br />
        - Tile layers & object layers<br />
        - Properties attachment (will be available from the component at "this.properties")<br />
        - Optional components to add to the entities, by specifying a <b>"components"</b> property to a tile/object.</p>
        
        <p>Restrictions: the map must be orthogonal & in JSON format</p>
        
        @memberof TiledMap#
        @param {string} mapUrl - The URL of the Tiled map (in JSON format).
        @param {function} callback - (Optional) A function to be called once the map is loaded.
        
        @example
        Crafty.e('TiledMap').tiledMap('assets/map.json');
        @example
        Crafty.e('TiledMap').tiledMap('assets/map.json',
            function(tiledMap) {
                console.log(tiledMap);
            });
     */
    tiledMap: function(mapUrl, callback) {
        this._renderType = 'DOM';
        var t1 = new Date().getTime(); // DEBUG: Benchmarking

        // Fetch level JSON
        $.ajax({
            url: mapUrl,
            dataType: 'json',
            context: this,
			cache: true,
            success: function(mapJson) {
				// GAME : Create background
			/*		c.e('2D, DOM, Image, Parallax')
						.image('assets/world' + (gameState.world+1) + 'bg.png');
					c.e('2D, DOM, Image, Parallax')
						.attr({x: 768})
						.image('assets/world' + (gameState.world+1) + 'bg.png');*/
				$('#cr-stage').css('background', "url('assets/world" + (gameState.world+1) + "bg.png')");
			
                // Compute assets path
                var fileNameRegexp = /\/[^\/]*$/;
               /* var assetPath;
                if (mapUrl.indexOf('://') != -1) {
                    assetPath = mapUrl.replace(fileNameRegexp, '/');
                }
                else {
                    assetPath = window.location.href.replace(fileNameRegexp, '/');
                    if (mapUrl.indexOf('/') != -1) {
                        assetPath += mapUrl.replace(fileNameRegexp, '/');
                    }
                }*/
				var assetPath = 'assets/';

                // Build tilesets
                this._buildTileSets(assetPath, mapJson);

                // Build layers
                this._buildTileLayers(mapJson);

               // console.log("Map loaded in " + (new Date().getTime() - t1) + "ms"); // DEBUG: Benchmarking
				//console.log("Nombre d'objets 2D: "+c("2D"));
				//console.log(c("2D").length);
                // Callback hook
                if (callback) callback(this);
            }
    
        });

    },

    _buildTileSets: function(assetPath, mapJson) {
        var tileSetKey, tilePropertiesKey, tileSetJson, map;

        for (tileSetKey in mapJson.tilesets) {
            tileSetJson = mapJson.tilesets[tileSetKey];

            // Create tile set
            map = {};
            map[tileSetJson.name] = [0, 0];
            Crafty.sprite(tileSetJson.tilewidth, assetPath + tileSetJson.image, map);

            // Store tileset info
            this._tileSetInfo.push({
            name: tileSetJson.name,
            min: tileSetJson.firstgid,
            max: tileSetJson.firstgid + (tileSetJson.imagewidth / tileSetJson.tilewidth) * (tileSetJson.imageheight / tileSetJson.tileheight),
            lineWidth: tileSetJson.imagewidth / tileSetJson.tilewidth,
			tileSize: tileSetJson.tilewidth
            });

            // Store tile types properties
            for (tilePropertiesKey in tileSetJson.tileproperties) {
                this._tileProperties[parseInt(tilePropertiesKey) + tileSetJson.firstgid] = tileSetJson.tileproperties[tilePropertiesKey];
            }
        }
    },

    _buildTileLayers: function(mapJson) {
        var tileLayerKey, tileLayerJson, dataKey, data, tileSetInfoKey, tileSetInfo, spriteName, newSprite, newObject;
		var technicalSprites = new Array();
        for (tileLayerKey in mapJson.layers) {
            tileLayerJson = mapJson.layers[tileLayerKey];

            if (tileLayerJson.type == "tilelayer") {
                for (dataKey in tileLayerJson.data) {
                    data = tileLayerJson.data[dataKey];
                    if (data != 0) {

                        for (tileSetInfoKey in this._tileSetInfo) {
                            tileSetInfo = this._tileSetInfo[tileSetInfoKey];
                            if (data >= tileSetInfo.min && data < tileSetInfo.max) {
                                break;
                            }
                        }

                        // Create sprite
						if (tileSetInfo.name == 'technical') { // JEU
							var x = dataKey % tileLayerJson.width;
							var y = Math.floor(dataKey / tileLayerJson.width);
							newSprite = technicalSprites[x+"|"+y];
							if(newSprite == null){
								newSprite = Crafty.e('2D, Collision')
									.attr({w: tileSetInfo.tileSize, h: tileSetInfo.tileSize, z: 110});
								technicalSprites[x+"|"+y] = newSprite;
							}
						}
						else {
							newSprite = Crafty.e('2D, Collision,  ' + this._renderType + ', ' + tileSetInfo.name);
							newSprite.sprite(
								((data - tileSetInfo.min) % tileSetInfo.lineWidth),
								Math.floor((data - tileSetInfo.min) / tileSetInfo.lineWidth)
							);
						}
                        newSprite.collision(new c.polygon([[0, 0], [newSprite.w, 0], [newSprite.w, newSprite.h], [0, newSprite.h]]));
                        newSprite.y = newSprite.h * Math.floor(dataKey / tileLayerJson.width);
                        newSprite.x = newSprite.w * (dataKey % tileLayerJson.width);
                        if (this._tileProperties[data]) {
                            // Attach properties
                            newSprite.properties = this._tileProperties[data];

                            // Attach components
                            if (this._tileProperties[data].components) {
                                newSprite.addComponent(this._tileProperties[data].components);
                            }
                        }
                    }
                }
            }
            
            else if (tileLayerJson.type == "objectgroup") {
                for (dataKey in tileLayerJson.objects) {
                    data = tileLayerJson.objects[dataKey];

                    newObject = Crafty.e('2D')
                    .attr({
                      x: data.x,
                      y: data.y,
                      w: data.width,
                      h: data.height
                      });
                    if (data.properties) {
                        // Attach properties
                        newObject.properties = data.properties;

                        // Attach components
                        if (data.properties.components) {
                            newObject.addComponent(data.properties.components);
                        }
                    }
                }
            }
        }
    }

});

});