define(function() {

    // WORK IN PROGRESS

    c.c('Trajectory', {
      init: function() {
        this.requires('2D');
        this.bind('EnterFrame', this._enterFrame);
      },
      trajectory: function() {
        switch (arguments[0]) {
          case 'points':
            // TODO Replace interpolation with speed?
            this.trajectory = new PointsTrajectory(arguments[1], arguments[2]); break;
        }
        return this;
      },
      _enterFrame: function() {
        if (this.trajectory) {
          this.attr(this.trajectory.next());
        }
      }
    });

    var AbstractArrayBasedTrajectory = Class.extend({
      init: function(points, interpolation) {
        this.steps = points || [[0, 0]];
        this.currentStepIndex = 0;
        this.currentStep = this.steps[0];
        this.currentInterpolationFrame = 0;
        this.interpolation = interpolation || c.timer.FPS();
      },
      next: function() {
        var nextStepIndex = (this.currentStepIndex + 1) % this.steps.length;
        
        // Switch to next step
        if (this.currentInterpolationFrame >= this.interpolation) {
          this.currentStepIndex = nextStepIndex;
          this.currentStep = this.steps[nextStepIndex];
          this.currentInterpolationFrame = 0;
        }
        
        var ratio = this.currentInterpolationFrame++ / this.interpolation;
        if (ratio == 0) {
          // Exact step
          return this.currentStep;
        }
        else {
          // Interpolated step
          return {
            x: this.currentStep.x*(1-ratio) + this.steps[nextStepIndex].x*ratio,
            y: this.currentStep.y*(1-ratio) + this.steps[nextStepIndex].y*ratio
          };
        }
      }
    });

    var PointsTrajectory = AbstractArrayBasedTrajectory.extend({
      init: function(points, interpolation) {
        this._super(points, interpolation);
      }
    });

});