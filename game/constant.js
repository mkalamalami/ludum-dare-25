define([], function() {

  return {
		SKIP_CLICK_TO_START: false,
		SKIP_INTRO: false,
		SKIP_MENU: false,
		SKIP_CUTSCENES: false, // true = buggy!
		SKIP_TO_VILLAIN: false, // true = buggy!
		DISABLE_SAVE: false,
		
		WIDTH: 768,
		HEIGHT: 576,
		
		RENDER: 'DOM',
		
		TILE_SIZE: 96,
		
		ASSETS: {
			PLAYER : 'assets/player.png',
			TILESET: 'assets/walls.png',
			OFFSET_WALL : 'assets/mur.png',
			EXIT_BTN : 'assets/exit.png',
			PIEGE_BG: 'assets/traps/piege_bg.png',
			PIEGE_1_MINI: 'assets/traps/piege1-mini.png',
			PIEGE_1: 'assets/traps/piege1.png',
			PIEGE_2_MINI: 'assets/traps/piege2-mini.png',
			PIEGE_2: 'assets/traps/piege2.png',
			PIEGE_3_MINI: 'assets/traps/piege3-mini.png',
			PIEGE_3: 'assets/traps/piege3.png',
			PIEGE_4_MINI: 'assets/traps/piege4-mini.png',
			PIEGE_4: 'assets/traps/piege4.png',
			LIFE_ON : 'assets/life_on.png',
			LIFE_OFF : 'assets/life_off.png',
			HURT_OVERLAY: 'assets/hurt_overlay.png',
			VILLAIN_OVERLAY: 'assets/villain_overlay.png',
			WORLD1BG: 'assets/world1bg.png',
			WORLD2BG: 'assets/world2bg.png',
			WORLD3BG: 'assets/world3bg.png',
			WORLD4BG: 'assets/world4bg.png',
			WORLD1MINI: 'assets/world1mini.png',
			WORLD2MINI: 'assets/world2mini.png',
			WORLD3MINI: 'assets/world3mini.png',
			WORLD4MINI: 'assets/world4mini.png',
			SOUND_ON: 'assets/soundon.png',
			SOUND_OFF: 'assets/soundoff.png',
			HELP: 'assets/help.png',
			HELP_ICON: 'assets/helpicon.png',
			DELAY_ICON: 'assets/delay.gif'
		},
		
		TRAPS: [ // trapIconType, trapType, previewComponent unless fixed trap, target type (or false for any)
			['Piege1Icon', 'Piege1', false, 'Top'],
			['Piege2Icon', 'Piege2', 'Piege2Hint', 'Left'],
			['Piege3Icon', 'Piege3', 'Piege3Hint', 'Right'],
			['Piege4Icon', 'Piege4', 'Piege4Hint', 'Bot']
		],
		
		WORLDS: [
			// Nom, position initiale
			['Manuwan Temple', [120,4*96 + 48], 'assets/world1.json', 4,
				[[5,0,0,10], [3,3,0,0], [1,2,0,2], [2,2,2,2]], 'They said it hides a diamond the size of a goat'
			],
			['Tower of Jujuk', [120,3*96 + 48], 'assets/world2.json', 5,
				[[1,3,1,1], [0,4,0,0], [0,1,2,2], [2,2,2,2],[2,2,2,2]], 'They said it hides a gem the size of a kitten'
			],
			['Taaz Pyramid', [100,3*96 + 48], 'assets/world3.json', 6,
				[[1,0,0,4], [1,1,1,1], [1,2,0,1], [0,0,0,4],[0,3,3,1],
					[2,2,2,2], [2,2,2,2], [2,2,2,2], [2,2,2,2],[2,2,2,2]], 'They said it hides a sapphire the size of a puppy'
			],
			['Ardkor Depths', [100,3*96 + 48], 'assets/world4.json', 8,
				[[1,0,0,4], [1,1,1,1], [1,2,0,1], [1,0,2,4],[0,3,3,1],
					[2,2,2,2], [2,2,2,2], [2,2,2,2], [2,2,2,2],[2,2,2,2],
					[2,2,2,2], [2,2,2,2], [2,2,2,2], [2,2,2,2],[2,2,2,2]], 'They said it hides a ruby the size of a horse-sized duck'
			],
			'assets/worldmenu.json'
		],
		
		SOUNDS: {
			MUSIC_INTRO: {
				ID: 'intro',
				URL: 'assets/music/intro.mp3',
				LOOP: false,
				MUSIC: true
			},
			MUSIC_INTRO_SHORT: {
				ID: 'introshort',
				URL: 'assets/music/introShort.mp3',
				LOOP: false,
				MUSIC: true
			},
			MUSIC_SPECIAL: {
				ID: 'special',
				URL: 'assets/music/special.mp3',
				LOOP: true,
				MUSIC: true
			},
			MUSIC_HERO: {
				ID: 'hero',
				URL: 'assets/music/hero.mp3',
				LOOP: true,
				MUSIC: true
			},
			MUSIC_VILLAIN: {
				ID: 'villain',
				URL: 'assets/music/villain.mp3',
				LOOP: true,
				MUSIC: true
			}
		},
		
		GAME:{
			OFFSET_SPEED: 200, // px/s
			TRAP_HINT: 1000, // ms
			GRAVITY: .8
		},
		
		MUSIC_VOLUME: 80
		
	}
});
