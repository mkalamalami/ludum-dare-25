define(['constant', 'traps'], function(Const) {

/**
Chaque instance de cr�ature doit :
- Avoir un composant de SpriteSheet
- Contr�ler this._direction et this._speed
*/
c.c('Creature', {
	init: function() {
		this.requires('2D, SpriteAnimation, Keyboard, ' + Const.RENDER);
		this.reel('Right', 700, [[2,0],[3,0],[4,0],[4,0],[3,0],[2,0],[1,0],[0,0],[0,0],[1,0]])
			.reel('Left', 700, [[2,1],[3,1],[4,1],[4,1],[3,1],[2,1],[1,1],[0,1],[0,1],[1,1]])
			.reel('Jump-Right-up', 1000, 6, 0, 1)
			.reel('Jump-Right-down', 1000, 7, 0, 1)
			.reel('Jump-Left-up', 1000, 6, 1, 1)
			.reel('Jump-Left-down', 1000, 7, 1, 1)
			.reel('Hit-Right', 1000, 8, 0, 1)
			.reel('Hit-Left', 1000, 8, 1, 1);
		this.bind('EnterFrame', this._enterFrame);
		
		this._spriteDirection = 1;
		this._direction = 0;
		this._speed = [0,0];
		this._jumping = false;
		this._life = 1;
		this._hurtFor = 0;
	},
	creature: function(x, y) {
		this.attr({x: x, y: y});
		this.requires('Obstacle, Collision');
		this.collision(new Crafty.polygon([11,4],[33,4],[33,48],[11,48]));
		return this;
	},
	isDead: function() {
		return this._life <= 0 && this._hurtFor <= 0;
	},
	_enterFrame: function(){
		if (this._direction != 0) {
			this._spriteDirection = this._direction;
		}
  
		if (this._hurtFor > 0) {
			this._hurtFor--;
      this.pauseAnimation();
			this.sprite(8, this._spriteDirection == -1 ? 1 : 0, 1, 1);
		}
		else if(this._direction == 0) {
			if (!this._jumping) {
        this.pauseAnimation();
				this.sprite(5, this.__coord[1] / 48, 1, 1);
			} else if(this._speed[1] < 0) {
				if(this._spriteDirection == 1)
					this.animate('Jump-Right-up', -1);
				else
					this.animate('Jump-Left-up', -1);
			} else {
				if(this._spriteDirection == 1)
					this.animate('Jump-Right-down', -1);
				else
					this.animate('Jump-Left-down', -1);
			}
		} else if(this._direction == 1) {
			if (!this._jumping) {
        if (this.reel() != 'Right') {
          this.animate('Right', -1);
        }
			}else if(this._speed[1] < 0){
				this.animate('Jump-Right-up', -1);
			}else{
				this.animate('Jump-Right-down', -1);
			}
		}else if(this._direction == -1){
			if (!this._jumping) {
        if (this.reel() != 'Left') {
          this.animate('Left', -1);
        }
      } else if (this._speed[1] < 0) {
				this.animate('Jump-Left-up', -1);
			} else {
				this.animate('Jump-Left-down', -1);
			}
		}
		
		// Harmful collisions
		var hitHarmfuls = this.hit('Harmful');
		if (hitHarmfuls) {
			_.each(hitHarmfuls, function(hitHarmful) {
				this._hit(hitHarmful.obj);
			}, this);
		}
		if (!this.has('Hero')) {
			var hitHeros = this.hit('Hero');
			if (hitHeros) {
				_.each(hitHeros, function(hitHero) {
					hitHero.obj._hit(this);
				}, this);
			}
		}
		
		// Death
		if (this._life <= 0 && this._hurtFor <= 0 && !this.has('Hero')) {
			this.destroy();
		}
		if(this._y > Const.HEIGHT){
			if(this.has('Hero')){
				this._life = 0;
			}else{
				this.destroy();
			}
		}
	},
	_hit: function(attacker) {
		if (attacker.has('Instakill')) {
			this._life = 0;
		}
		else {
			this._life--;
		}
		attacker.removeComponent('Harmful');
		if (this._hurtFor == 0) {
			this._hurtFor = 20; // Hurt delay
			if (this._speed[1] >= 0 && this.has('HeroPlayer')) {
				this._speed[1] -= 5; // Small jump
			}
		}
	}
});

});