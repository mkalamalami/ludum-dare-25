window.onload = function() {

// Define scripts paths
requirejs.config({
    baseUrl: 'game',
    paths: {
        lib: '../lib',
        c: '../components'
    }
});

// Load libraries
require(['constant', 'lib/jquery', 'lib/crafty', 'lib/underscore',
		'lib/soundmanager2'], function(Const) {
    $(document).ready(function() {
		// Init Crafty
        window.c = Crafty;
		c.init(Const.WIDTH, Const.HEIGHT);
		document.getElementById('cr-stage').focus();
		// Init model
		window.gameState = {
		
			// Current played level
			world: 3,
			level: 1,
			lives: 3,
			villainMode: false,
			replay: [[],[]], // playerReplayData, trapReplayData
			
			// Game progress
			unlockedWorld: 0,
			firstRun: true,
            firstVillainRun: true,
			
			// Settings
			muted: false,
			
			// Replays storage
			replays: [], // {date, world, level, replayData}
			replayToRun: []  // {date, world, level, replayData}
		};
		
		// Launch initial scene
		require(['scene_load', 'lib/jstorage'], function() {
		
			// Load model
			if (!Const.DISABLE_SAVE) {
				var load = $.jStorage.get('gameState');
				if (load) {
					window.gameState = load;
				}
				
				if (gameState.muted) {
					soundManager.mute();
				}
			}
		
			c.scene('Load');
		});
    });
});

};