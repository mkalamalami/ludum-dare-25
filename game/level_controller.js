define(['constant', 'utils'], function(Const, Utils) {
	
	c.c("LevelController", {
		init : function(){
			this.requires('2D');
			this.bind("EnterFrame",this._enterFrame);
			this.bind('ObstacleClick', this._createTrap);
			this.bind('ObstacleOver', this._enterObstacle);
			this.bind('ObstacleOut', this._outObstacle);
			this._speed = Const.GAME.OFFSET_SPEED / c.timer.FPS();
			this._lastClick = 0;
			this.replay = false;
			this.selectedIcon = null; // icons set themselves when clicked
		},
		
		levelController: function(hero) {
			if(gameState.villainMode){
				this.decalage = Const.WIDTH/5;
				var controller = this;
				$(document).mousemove(function(e){
					controller._previewTrap(e);
				}); 
				
			}else{
				this.decalage = Const.WIDTH/3;
			}
			this.hero = hero;
			this.lifeView = c.e("LifeView");
			this.lifeView.lifeView(hero);
			
			this.offset_wall = c.e("2D,Image,"+Const.RENDER+",Harmful,Instakill")
								.image(Const.ASSETS.OFFSET_WALL)
								.requires('Obstacle');
			this.offset_wall.x = -500;
			this.levelView = c.e("LevelView");
			return this;
		},
		
		kill: function() {
			this.offset_wall.destroy();
			this.lifeView.destroy();
			this.levelView.destroy();
			return this;
		},
		
		_enterFrame: function() {
			// Avancement mur
			this.offset_wall.x += this._speed;
			
			c.viewport.x = Math.min(this.decalage - this.hero._x, - this.offset_wall.x - 150);
			c.viewport.x = Math.min(0, c.viewport.x);
			
			// Fixed items
			var fixedItems = c('FixedItem');
			for (var i = 0; i < fixedItems.length; i++) {
				var fixedItem = c(fixedItems[i]);
				fixedItem.x = fixedItem.screenX - c.viewport.x;
			}
		},
		
		_createTrap: function(obstacleClicEvent) {
			var curTime = new Date().getTime();
			if (curTime - this._lastClick > 300) {
				if (this.selectedIcon) {
					var success = this.selectedIcon.createTrap(obstacleClicEvent);
					if (success && gameState.villainMode) {
						this.hero.registerTrapCreation(obstacleClicEvent, this.selectedIcon);
					}
					this._lastClick = curTime;
				}
			}
		},
		_previewTrap: function(event) {
			if (this.selectedIcon) {
				this.selectedIcon.showPreview(event);
			}
		},
		_enterObstacle: function(event){
			if (this.selectedIcon) {
				this.selectedIcon.showPreview(event,c(parseInt(event.target.id.substr(3))));
			}
		},
		_outObstacle: function(){
			if (this.selectedIcon) {
				this.selectedIcon.out();
			}
		}
	});
	
	c.c("LifeView",{
		init: function(){
			this.bind("EnterFrame", this._enterFrame);
		},
		lifeView: function(hero){
			this.hero = hero;
			this.lifeArray = new Array();
			this.nblife = hero.getLife();
			this.maxLife = this.nblife;
			for(var i=0; i < this.maxLife; i++){
				if(this.hero.getLife() > i){
					this.lifeArray[i] = c.e("2D,Image,FixedItem,"+Const.RENDER).attr({z: 53}).fixedItem(Const.WIDTH - 40 - i* 25 ,20).image(Const.ASSETS.LIFE_ON);
				}else{
					this.lifeArray[i] = c.e("2D,Image,FixedItem,"+Const.RENDER).attr({z: 53}).fixedItem(Const.WIDTH - 40 - i* 25 ,20).image(Const.ASSETS.LIFE_OFF);
				}
			}
		},
		_enterFrame: function(){
			if(this.nblife != this.hero.getLife()){
				for(var i=0; i < this.maxLife; i++){
					if(this.hero.getLife() > i){
						this.lifeArray[i].image(Const.ASSETS.LIFE_ON);
					}else{
						this.lifeArray[i].image(Const.ASSETS.LIFE_OFF);
					}
				}
			}
		}
	});
	
	c.c("LevelView",{
		init: function(){
			c.e("2D,Text,FixedItem,"+Const.RENDER)
				.fixedItem(Const.WIDTH - 300 ,20)
				.text("World: "+ Const.WORLDS[gameState.world][0]+"<br />Level: "+gameState.level+"/"+Const.WORLDS[gameState.world][3])
				.attr({w:400, z:110});
		}
	});
	
});