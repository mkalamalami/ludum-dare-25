define(['constant', 'utils', 'cutscenes', 'creature', 'scene_villain'], function(Const, Utils, Cutscenes) {

c.c('Hero', {

	HERO_SIZE: [48, 48],
	HERO_OBSTACLE_SIZE: [32, 40],

	HORIZONTAL_ACC: 1.6,
	HORIZONTAL_FRICTION: 0.79,
	HORIZONTAL_FRICTION_IN_AIR: 0.98,
	JUMP_INITIAL_ACC: 4.5,
	JUMP_AIR_FRICTION: 0.7,
	
	PLAYER_LIFE: 1,
	REPLAY_PLAYER_LIFE: 3,
	
	init: function() {
		this.requires('PlayerSpriteSheet, Creature');
		this._jumpAcc = 0;
		this._downKeys = [false,false,false]; // left,right,jump
		this._life = this.PLAYER_LIFE;
		this.attr({w: this.HERO_SIZE[0], h: this.HERO_SIZE[1]});
		this._replayFrame = 0; // also controlled by replay
		this._hurtOverlay = c.e('2D,DOM,Image,FixedItem')
			.attr({z: 50 /*< technical objets*/, alpha: 0}).fixedItem(0,0).image(Const.ASSETS.HURT_OVERLAY);
	},
	
	hero: function(x, y, replayData) {
		this._trapsReplayDataCache = (replayData && replayData[1]) ? replayData[1] : [];
		this._trapsReplayData = this._trapsReplayDataCache.slice();
		this.creature(x, y);
		return this;
	},
	
	getLife: function() {
		return (this._life < 0) ? 0 : this._life;
	},
	
	_updateHero: function(pauseTraps) {
		// Obstacle handling
		this._jumping = true;
		var hit = this.hit('Obstacle');
		if (hit) {
			_.each(hit, function(obstacle) {
        if (!obstacle.normal) {
          return;
        };
        
				if (obstacle.normal.y != 0) {
					if (obstacle.normal.y == 1) {
						this._speed[1] = Math.max(this._speed[1], 0);
					}
					else {
						this._speed[1] = Math.min(this._speed[1], 0);
						this._jumping = false;
					}
					if (Math.abs(obstacle.overlap) > 1) {
						this.y -= obstacle.normal.y * obstacle.overlap * .3;
					}
				}
				else if (obstacle.normal.x != 0 && Math.abs(obstacle.overlap) > 1) {
					if (obstacle.normal.x == -1) {
						this._speed[0] = Math.min(this._speed[0], 0);
					}
					else {
						this._speed[0] = Math.max(this._speed[0], 0);
					}
					this.x += obstacle.normal.x * obstacle.overlap / -1.5;
				}
			}, this);
		}
		
		// Movement
		this.x += this._speed[0];
		this.y += this._speed[1];
		
		// Handle left/right
		this._direction = 0;
		if (this._downKeys[0]) {
			this._speed[0] -= this.HORIZONTAL_ACC;
			this._direction--;
		}
		if (this._downKeys[1]) {
			this._speed[0] += this.HORIZONTAL_ACC;
			this._direction++;
		}
		
		// Handle jump
		if (this._downKeys[2]) {
			if (!this._jumping) {
				this._jumpAcc = this.JUMP_INITIAL_ACC;
				this._speed[1] = 0;
			}
			else if (this._jumpAcc > 0.5) {
				this._jumpAcc *= this.JUMP_AIR_FRICTION;
			}
		}
		else if (this._jumpAcc > 0) {
			this._jumpAcc = 0;
			if (this._speed[1] < 0) {
				this._speed[1] = 0;
			}
		}
		
		// Gravity / Friction
		this._speed[0] *= (this._speed[1] != 0 && this._direction == 0) ? this.HORIZONTAL_FRICTION_IN_AIR : this.HORIZONTAL_FRICTION;
		if (this._jumping) {
			this._speed[1] += Const.GAME.GRAVITY;
		}
		this._speed[1] -= this._jumpAcc;
		
		// Extract and process trap data
		if (!pauseTraps) {
			_.each(this._trapsReplayData, function(trapData) {
				if (trapData[0] == this._replayFrame + Const.GAME.TRAP_HINT
					&& Const.TRAPS[trapData[2]][2] != false) {
					c.e(Const.TRAPS[trapData[2]][2]).trapHint(trapData[1]);
				}
				else if (trapData[0] == this._replayFrame) {
					c.e(Const.TRAPS[trapData[2]][1]).trap(trapData[1]);
				}
			}, this);
			this._replayFrame++;
		}
		
		// Hurt overlay
		if (this._hurtFor > 0) {
			this._hurtOverlay.alpha = this._hurtFor * 5 / 100;
		}
	}

});

c.c('HeroReplay', {
	init: function() {
		this.requires('Hero');
		this._pause = false;
		this._initialPos = [0,0];
		this._replayFrame = 0;
		this._life = this.REPLAY_PLAYER_LIFE;
	},
	heroReplay: function(x, y, replayData) {
		this._playerReplayDataCache = (replayData && replayData[0]) ? replayData[0] : [];
		this._initialPos = [x,y];
		this.hero(x, y, replayData);
		this.restartReplay();
		this.bind('EnterFrame', this._updateHeroReplay);
		return this;
	},
	restartReplay: function() {
		this._playerReplayData = this._playerReplayDataCache.slice(); // [[posx,posy],[spx,spy],dir]
		this._trapsReplayDataOverride = this._trapsReplayDataCache.slice(); // [[frame, event, trapId]...]
		this._trapsReplayData = this._trapsReplayDataCache.slice(); 
		this.x = this._initialPos[0];
		this.y = this._initialPos[1];
		this._jumpAcc = 0;
		this._downKeys = [false,false,false];
		this._speed = [0, 0];
		this._direction = 0;
		this._replayFrame = 0;
		this.pause(false);
		return this;
	},
	pause: function(pause) { // no arg = toggle
		this._pause = (pause !== undefined) ? pause : !this._pause;
		return this;
	},
	registerTrapCreation: function(obstacleClicEvent, trapIcon) {
		this._trapsReplayDataOverride.push([
			(trapIcon.isFixedTrap()) ? 0 : this._replayFrame,
			obstacleClicEvent,
			trapIcon.getTrapId()
		]);
	},
	_updateHeroReplay: function() {
		if (!this._pause) {
			// Extract replay data
			var playerData = this._playerReplayData.shift();
			if (playerData) {
				this._downKeys = playerData.slice();
			}
		}
		
		// Update
		this._updateHero(this._pause);
		
		// In this mode, Hero must not finish alive
		if (this.hit('Win')) {
			c.scene('Villain');
		}
		if (this.isDead()) {
			// Success
			Utils.stopAllSound();
			gameState.replay = [this._playerReplayDataCache, this._trapsReplayDataOverride];
			c.scene('Hero');
		}
	}
});

c.c('HeroPlayer', {
	init: function() {
		this.requires('Hero');
		this._playerReplayData = [];
	},
	heroPlayer: function(x, y, replayData) {
		this.hero(x, y, replayData);
		this.requires('Keyboard');
		this.bind('EnterFrame', this._updateHeroPlayer);
		return this;
	},
	getReplayData: function() {
		return this._playerReplayData;
	},
	registerTrapCreation: function(obstacleClicEvent, trapIcon) {
        // NOTHING
	},
	_updateHeroPlayer: function() {
		// Input
		if (this._life > 0) {
			this._downKeys[0] = this.isDown('Q') || 
					this.isDown('A') ||
					this.isDown('LEFT_ARROW');
			this._downKeys[1] = this.isDown('D') || 
					this.isDown('RIGHT_ARROW');
			this._downKeys[2] = this.isDown('SPACE') || 
					this.isDown('UP_ARROW') ||
					this.isDown('Z') ||
					this.isDown('W');
		}
		else {
			this._downKeys = [false,false,false];
		}
		
		// Update
		this._updateHero(false);
			
		// Save replay data
		this._playerReplayData.push(this._downKeys.slice());
	
		// Victory/Death
		if (this.hit('Win')) {
			Utils.stopAllSound();
			gameState.replay = [this._playerReplayData, this._trapsReplayDataCache];
			gameState.level++;
			
			c('LevelController').kill().destroy();
			var hero = c.e('HeroReplay').heroReplay(this.x, this.y);
			this.destroy();
			this.hero = hero;
			this.cinema = c.e('Cinema');
			this.camera = c.e('FollowHero').followHero(this.hero);
			this.resume = function() {
				if (gameState.level > Const.WORLDS[gameState.world][3]){
					if(gameState.world == gameState.unlockedWorld){
						gameState.unlockedWorld++;
					}
					gameState.level = 0;
					c.scene('Menu');
				} else {
					c.scene('Villain');
				}
			}
			
            c('Win').destroy();
			if (gameState.level > Const.WORLDS[gameState.world][3]){
				Cutscenes.worldEnd(this);
			} else if (gameState.level == 2) {
				Cutscenes.firstLevelEnd(this);
			}
			else {
				Cutscenes.levelEnd(this);
			}
		}
		else if (this.isDead()) {
			c.scene('Hero');
		}
	}
});

});