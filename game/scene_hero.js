define(['constant', 'utils', 'cutscenes', 'c/tiledMap','c/script', 'player', 'level_controller'], function(Const,Utils,Cutscenes) {

	c.scene('Hero', function() {
	    // Chargement Map
		Utils.resetViewport();
		var mask = c.e('Mask').mask(true);
		gameState.villainMode = false;
		
		// Start level
		c.e('TiledMap').tiledMap(Const.WORLDS[gameState.world][2], function() {
			mask.triggerMask(30);
			
			// Level elements
			this.initialPos = Const.WORLDS[gameState.world][1];
			this.hero = c.e('HeroReplay').heroReplay(this.initialPos[0], this.initialPos[1]);
			this.camera = c.e('FollowHero').followHero(this.hero);
			this.resume = function() {
				Utils.playMusic(Const.SOUNDS.MUSIC_HERO);
				var actualHero = c.e('HeroPlayer').heroPlayer(this.initialPos[0], this.initialPos[1], gameState.replay);
				this.camera.destroy();
				this.hero.destroy();
				c.e('LevelController').levelController(actualHero);
			}
			this.cinema = c.e('Cinema');
			
			// Run start cutscene
			if (Const.SKIP_CUTSCENES) {
				this.cinema.endCinema();
				this.resume();
			}
			else {
				if (gameState.level == 1) {
					Cutscenes.worldStart(this, true);
				}
				else {
					Cutscenes.levelStart(this, true);
				}
			}
		});
		
		c.e('ExitToMenu');
		c.e('MuteButton, FixedItem')
			.attr({z: 200})
			.fixedItem(50, 530)
			.setMusic(Const.SOUNDS.MUSIC_HERO);
		
	});
   
});