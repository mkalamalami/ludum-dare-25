define(['constant', 'utils', 'c/script', 'scene_menu'], function(Const, Utils) {
	
	c.scene('Intro', function() {
  
		if (Const.SKIP_INTRO /*|| !gameState.firstRun*/) {
			c.scene('Menu');
			return;
		}
		
		c.c('IntroTitle', {
			init: function() {
				this.requires('2D, DOM, Text');
				this.css({'font-size': '45pt'});
			}
		});
		c.c('IntroSmallTitle', {
			init: function() {
				this.requires('2D, DOM, Text');
				this.css({'font-size': '25pt'});
			}
		});
		c.c('IntroText', {
			init: function() {
				this.requires('2D, DOM, Text');
				this.css({'font-size': '17pt', 'font-style': 'italic'});
			}
		});
		
		this.createText = function(comp, text, x, y) {
			c.e(comp).text(text).attr({x: x, y: y, w: 800});
		}
		this.clearScreen = function() {
			c('IntroTitle, IntroText, IntroSmallTitle').destroy();
		}
		
		var scene = this;
		var introMusic = soundManager.getSoundById(Const.SOUNDS.MUSIC_INTRO.ID);
		introMusic.onPosition(1, function() {
			var bar = 710;
			
			var bg = c.e('2D,DOM,Image,Tween');
			bg.alpha = 0.;
			bg.image(Const.ASSETS.WORLD4BG);
			bg.tween({alpha: 1.}, bar * 16);
			
			c.e('Script')
				.action(bar * 0, scene, 'createText', 'IntroTitle', 'Manu', 210, 200)
				.action(bar * 1, scene, 'createText', 'IntroTitle', '& Wan', 380, 200)
				.action(bar * 1, scene, 'createText', 'IntroText', 'present', 340, 275)
				.action(bar * 1.5, scene, 'clearScreen')
				.action(bar * 0.5, scene, 'createText', 'IntroSmallTitle', 'A Ludum Dare #25 game', 180, 200)
				.action(bar * 3, scene, 'clearScreen')
				.action(bar * 1, scene, 'createText', 'IntroSmallTitle', 'With a mind-bending gameplay', 105, 200)
				.action(bar * 2, scene, 'createText', 'IntroText', '...and', 280, 300)
				.action(bar * 2, scene, 'createText', 'IntroText', 'a weird story', 380, 300)
				.action(bar * 2, scene, 'clearScreen')
				.action(bar * 1, scene, function() { c.scene('Menu'); })
				.run(false, false);
		});
		introMusic.play();
	
	});
   
});
