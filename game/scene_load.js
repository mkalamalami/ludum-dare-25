define(['constant'], function(Const) {

c.scene('Load', function() {

	var BAR_WIDTH = 200, BAR_HEIGHT = 30;
	
	// Label
	var label = c.e('2D, DOM, Text')
		.attr({x: (Const.WIDTH - BAR_WIDTH)/2, y: (Const.HEIGHT - BAR_HEIGHT)/2 - BAR_HEIGHT, w: BAR_WIDTH, h: BAR_HEIGHT})
		.text('Loading graphics...');
	
	// Contour
	c.e('2D, DOM')
		.attr({x: (Const.WIDTH - BAR_WIDTH)/2, y: (Const.HEIGHT - BAR_HEIGHT)/2, w: BAR_WIDTH, h: BAR_HEIGHT})
		.css({'border': '1px solid white'});
		
	// Fill
	var fill = c.e('2D, DOM')
		.attr({x: (Const.WIDTH - BAR_WIDTH)/2, y: (Const.HEIGHT - BAR_HEIGHT)/2, w: 0, h: BAR_HEIGHT + 2})
		.css({'background-color': 'white'});
		
	// Assets loading
	c.load(_.values(Const.ASSETS), function() {
		require(['assets'], function() {
			
			// Sounds loading
			label.text('Loading sounds...');
			soundManager.setup({
				preferFlash: true,
				onready: function() {
					fill.w = 0;
					var soundCount = _.size(Const.SOUNDS) - 1, loadedSounds = -1;
					_.each(Const.SOUNDS, function(sound) {
						soundManager.createSound({
						  id: sound.ID,
						  url: sound.URL,
						  loops: (sound.LOOP) ? 999 : 0,
						  volume: (sound.MUSIC) ? Const.MUSIC_VOLUME : 100,
						  onload: function() {
							loadedSounds++;
							var percentage = 100 * loadedSounds / soundCount;
							fill.w = percentage * BAR_WIDTH / 100 + 2;
							
							if (loadedSounds == soundCount) {
								label.text('Almost there...');
							
								// Menu scene
								require(['scene_intro'], function() {
									label.text('Ready.');
									if (Const.SKIP_CLICK_TO_START) {
										c.scene('Intro');
									}
									else {
										c.e('TextButton')
											.attr({x: Const.WIDTH/2 - 100, y: 330, w: 200, h: 40})
											.css({border: '1px solid white'})
											.textButton('Click here to begin', function() {
												c.scene('Intro');
											}, 17, 10);
									}
								});
							}
						  }
						}).load();
					});
				}
			});
		});
	},
	function(e) {
		fill.w = e.percent * BAR_WIDTH / 100 + 2;
	});

});

/*    
      // Init sound and load mp3s
      c.scene('loadsounds', function() {
        soundsLoadBaseText = "Loading sounds: ";
        var soundCount = _.size(consts.SOUNDS), loadedSounds = 0;
        var soundLoadMessage = c.e('2D, DOM, Text, LoadingMessage')
          .attr({x: 0, y: consts.HEIGHT/2 - 20, w: consts.WIDTH, h: 40})
          .text(soundsLoadBaseText + "<b>0%</b>");
        _.each(consts.SOUNDS, function(sound) {
            var isMusic = sound.ID.indexOf('music') != -1;
            soundManager.createSound({
              id: sound.ID,
              url: sound.URL,
              loops: (isMusic) ? 999 : 0,
              volume: (isMusic) ? consts.MUSIC_VOLUME : 100,
              onload: function() {
                loadedSounds++;
                soundLoadMessage.text(soundsLoadBaseText + "<b>" + Math.floor(100 * loadedSounds / soundCount) + "%</b>");
                if (loadedSounds == soundCount) {
                  c.scene('loadassets');
                }
              }
            }).load();
        });
      });
      soundManager.setup({
        url: './sound/swf',
        preferFlash: true,
        onready: function() {
          c.scene('loadsounds');
        }
      });
      if (gameState.mute) {
        soundManager.mute();
      }*/

});