define(['constant', 'utils', 'scene_hero'], function(Const, Utils) {

	c.scene('Menu', function() {
		Utils.resetViewport();
		c.viewport.x = c.viewport.y = 0;
		
		// Music
		if (gameState.firstRun && !Const.SKIP_INTRO) {
			gameState.firstRun = false;
		}
		else {
			Utils.playMusic(Const.SOUNDS.MUSIC_INTRO_SHORT);
		}
		
		// Save
		$.jStorage.set('gameState', gameState);
		
		// Background
		c.e('TiledMap').tiledMap(Const.WORLDS[4], function() {
			$('#cr-stage').css('background', "url('assets/world1bg.png')");
		
			// Game title
			c.e('2D, DOM, Text')
				.attr({x: 240, y: 20, w: 300})
				.text('Sisyphus')
        .textFont({size: '50pt', family: "perfect_dos_vga_437_winRg" });
			c.e('2D, DOM, Text')
				.attr({x: 280, y: 100, w: 300})
				.text('and the Cursed Diamonds')
        .textFont({size: '16px', family: "perfect_dos_vga_437_winRg" });

			// World buttons
			for (var i = 0; i < 4; i++) {
				var label = c.e('2D, DOM, Text')
					.attr({x: 92 + i * 150, y: 184 + (i%2)*169, w: 150})
					.text(Const.WORLDS[i][0]);
				var thumb = c.e('2D, DOM, Image, Mouse');
				thumb.worldId = i;
				thumb.image('assets/world' + (i+1) + 'mini.png')
					.css({'border': '2px solid white'})
					.attr({x: 92 + i * 150, y: 210});
				if (gameState.unlockedWorld >= i) {
					thumb.requires('Pointer')
						.bind('Click', function() {
							gameState.world = this.worldId;
							gameState.level = 1;
							gameState.villainMode = false;
							gameState.replay = [];
							
							c.e('Mask').mask().triggerMask(null, function() {
								if (Const.SKIP_TO_VILLAIN) {
									c.scene('Villain');
								}
								else {
									c.scene('Hero');
								}
							});
						});
				}
				else {
					label.alpha = 0.2;
					thumb.alpha = 0.2;
				}
			}
			
			// Reset game button
			c.c('ResetButton', {
				init: function() {
					this.requires('2D, DOM, Mouse, Pointer')
						.attr({w: 300, h: 50});
					this._label = c.e('2D, DOM, Text, Pointer')
						.attr({w: this.w, h: this.h})
						.text('Reset all progress')
						.bind('Click', function() {
							this.parent.click();
						});
					this.attach(this._label);
					this.attr({x: 550, y: 540});
					this.bind('Click', this._click);
					this.confirm = false;
				},
				_click: function() {
					if (!this.confirm) {
						this._label.text('Click again to confirm');
						this.confirm = true;
					}
					else {
						gameState.unlockedWorld = 0;
						c.scene('Menu');
					}
				}
		
			});
			c.e('ResetButton');
			
			// Mute/unmute button
			c.e('MuteButton').attr({x: 20, y: 530});
			
			// Fade in
			c.e('Mask').mask(true).triggerMask(800);
			
			if (Const.SKIP_MENU) {
				gameState.world = 0;
				gameState.level = 1;
				gameState.villainMode = false;
				gameState.replay = [];
				if (Const.SKIP_TO_VILLAIN) {
					c.scene('Villain');
				}
				else {
					c.scene('Hero');
				}
			}
		});
	});
});
