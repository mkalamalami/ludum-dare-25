define(['constant', 'utils', 'c/tiledMap', 'player', 'level_controller'], function(Const, Utils) {

	c.scene('Replay', function() {
	    // Chargement Map
		Utils.resetViewport();
		var mask = c.e('Mask').mask(true);
		c.e('TiledMap').tiledMap(Const.WORLDS[gameState.replayToRun[1]][2], function() {
			mask.triggerMask(30);
			
			var initialPos = Const.WORLDS[gameState.replayToRun[1]][1];
			var hero = c.e('HeroReplay').heroReplay(initialPos[0], initialPos[1], gameState.replayToRun[3]);
			var levelController = c.e('LevelController').levelController(hero);
			
			// TODO Replay control + Continue button
		});
		
		c.e('ExitToMenu');
	});
   
});