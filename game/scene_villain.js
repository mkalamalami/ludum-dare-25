define(['constant', 'utils', 'cutscenes', 'c/tiledMap', 'c/script',
	'player', 'level_controller', 'utils', 'traps'], function(Const, Utils, Cutscenes) {
  
	c.scene('Villain', function() {
	    // Chargement Map
		Utils.resetViewport();
		var mask = c.e('Mask').mask(true);
		var worldData = Const.WORLDS[gameState.world];
		gameState.villainMode = true;
		
		c.e('TiledMap').tiledMap(worldData[2], function() {
			mask.triggerMask(30);
			
			c.e('2D,DOM,Image,FixedItem')
				.attr({z: 50 /*< technical objets*/})
				.fixedItem(0,0).image(Const.ASSETS.VILLAIN_OVERLAY);
			
			// Level elements
			this.initialPos = worldData[1];
			this.hero = c.e('HeroReplay').heroReplay(this.initialPos[0], this.initialPos[1]);
			this.camera = c.e('FollowHero').followHero(this.hero);
			this.resume = function() {
				Utils.playMusic(Const.SOUNDS.MUSIC_VILLAIN);
				var actualHero;
				if (Const.SKIP_TO_VILLAIN) {
					gameState.level++;
					actualHero = c.e('HeroPlayer').heroPlayer(this.initialPos[0], this.initialPos[1]);
				}
				else {
					actualHero = c.e('HeroReplay').heroReplay(this.initialPos[0], this.initialPos[1], gameState.replay);
				}
				this.camera.destroy();
				this.hero.destroy();
				var levelController = c.e('LevelController').levelController(actualHero);
				
				// Villain UI : Selected icon
				var selectedIcon = c.e('2D, Image, FixedItem, ' + Const.RENDER);
				selectedIcon.levelController = levelController;
				selectedIcon.fixedItem(-200,25)
					.image(Const.ASSETS.PIEGE_BG)
					.attr({z: 51})
					.bind('EnterFrame', function() {
						var selected = this.levelController.selectedIcon;
						if (selected) {
							this.setX(selected.screenX - 5);
						}
					});
				
				// Villain UI : Icons
				var x = 55, y = 30, i=0;
				_.each(worldData[4][gameState.level - 2], function(amount, trapId) {
					if (amount > 0) {
						i++;
						var trapIconComponent = Const.TRAPS[trapId][0];
						var trapIcon = c.e(trapIconComponent).trapIcon(levelController, trapId, x, y, amount).listenKeyBoard(i,levelController).attr({z: 1100});
						if (!levelController.selectedIcon) {
							levelController.selectedIcon = trapIcon;
						}
						x += 70;
					}
				});
			}
			
			this.cinema = c.e('Cinema');
			
			c.e('ExitToMenu');
            c.e('HelpIcon');
			c.e('MuteButton, FixedItem')
				.attr({z: 200})
				.fixedItem(90, 530)
				.setMusic(Const.SOUNDS.MUSIC_VILLAIN);
			
			// Run start cutscene
			if (Const.SKIP_CUTSCENES) {
				this.cinema.endCinema();
				this.startLevel();
			}
			// Le lvl monte après un e victoire en normal, on arrive donc au lvl 2 au 1er mode villain
			else if (gameState.world == 0 && gameState.level == 2 && gameState.firstVillainRun) {
                gameState.firstVillainRun = false;
				Cutscenes.villainTuto(this);
			}
			else {
				Cutscenes.villainStart(this);
			}
		});
		
	});
	
});