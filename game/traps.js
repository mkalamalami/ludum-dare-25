define(['constant','utils'], function(Const, Utils) {

// Tiles

//Source: http://stackoverflow.com/questions/8389156/what-substitute-should-we-use-for-layerx-layery-since-they-are-deprecated-in-web
function getOffset(evt) {
  var el = evt.target,
      x = y = 0;

  while (el && !isNaN(el.offsetLeft) && !isNaN(el.offsetTop)) {
    x += el.offsetLeft - el.scrollLeft;
    y += el.offsetTop - el.scrollTop;
    el = el.offsetParent;
  }

  x = evt.clientX - x;
  y = evt.clientY - y;

  return { x: x, y: y };
}

c.c('Obstacle', {
	init: function() {
		this.requires('Collision,Mouse,Pointer,'+Const.RENDER);
		this.collision();
		return this;
	}
});

c.c('TopTrap',{
	init: function(){
		this.requires('Mouse,Pointer,'+Const.RENDER);
		if(gameState.villainMode){
			this.css('cursor','pointer');
			this.bind('MouseDown', function(event) {
				Crafty.trigger('ObstacleClick', {
					x: getOffset(event).x,
					y: this.y,
					position: 'Top'
				});
			});
			this.bind('MouseOver', function(event) {
				Crafty.trigger('ObstacleOver', event);
			});
			this.bind('MouseOut', function(event) {
				Crafty.trigger('ObstacleOut');
			});
		}
	}
});

c.c('LeftTrap',{
	init: function(){
		this.requires('Mouse,Pointer,'+Const.RENDER);
		if(gameState.villainMode){
			this.css('cursor','pointer');
			this.bind('MouseDown', function(event) {
				Crafty.trigger('ObstacleClick',{
					x: this.x,
					y: getOffset(event).y,
					position: 'Left'
				});
			});
			this.bind('MouseOver', function(event) {
				Crafty.trigger('ObstacleOver', event);
			});
			this.bind('MouseOut', function(event) {
				Crafty.trigger('ObstacleOut');
			});
		}
	}
});

c.c('RightTrap',{
	init: function(){
		this.requires('Mouse,Pointer,'+Const.RENDER);
		if(gameState.villainMode){
			this.css('cursor','pointer');
			this.bind('MouseDown', function(event) {
				Crafty.trigger('ObstacleClick',{
					x: this.x,
					y: getOffset(event).y,
					w: this.w,
					position: 'Right'
				});
			});
			this.bind('MouseOver', function(event) {
				Crafty.trigger('ObstacleOver', event);
			});
			this.bind('MouseOut', function(event) {
				Crafty.trigger('ObstacleOut');
			});
		}
	}
});

c.c('BotTrap',{
	init: function(){
		this.requires('Mouse,Pointer,'+Const.RENDER);
		if(gameState.villainMode){
			this.css('cursor','pointer');
			this.bind('MouseDown', function(event) {
				Crafty.trigger('ObstacleClick',{
					x: getOffset(event).x,
					y: this.y,
					h: this.h,
					position: 'Bot'
				});
			});
			this.bind('MouseOver', function(event) {
				Crafty.trigger('ObstacleOver', event);
			});
			this.bind('MouseOut', function(event) {
				Crafty.trigger('ObstacleOut');
			});
		}
	}
});

// Actual traps

c.c('TrapIcon', {
	init: function() {
		this.requires("2D, Image, FixedItem, Text, Mouse, Keyboard, Pointer, " + Const.RENDER);
		this.bind('MouseDown', this._select);
		this.textFont({ size: '20px', weight: 'bold' }).textColor('#FFE900');
	},
	trapIcon: function(levelController, id, x, y, amount) {
		this._levelController = levelController;
		this._trapId = id;
		this.supportedTarget = Const.TRAPS[this._trapId][3];
		this._amount = amount;
		this.fixedItem(x, y);
		this.text(amount);
		this.attr({w: 50, h: 50});
		this.css({'background-color': 'white'});
		return this;
	},
	listenKeyBoard: function(key,levelController){
		this.bind('KeyDown', function(e) {
			if(e.key == Crafty.keys[key] || e.key == Crafty.keys['NUMPAD_'+key]) {
			  levelController.selectedIcon = this;
			}
		});
		return this;
	},
	createTrap: function(obstacleClicEvent) {
		if (this._amount > 0 && (obstacleClicEvent.position == this.supportedTarget)) {
			c.e(Const.TRAPS[this._trapId][1]).trap(obstacleClicEvent);
			this._amount--;
			this.text(this._amount);
			if (this._amount == 0) {
				this.textColor('#FF0000');
			}
			return true;
		}
		else {
			return false;
		}
	},
	showPreview: function(event,overElem){
		if(overElem){
			if(overElem.has(this.supportedTarget+'Trap')){
			  offset = getOffset(event)
				this.preview.over({
					x: overElem.x,
					y: overElem.y ,
					layerX : offset.x,
					layerY : offset.y,
					pageX : event.pageX,
					pageY : event.pageY,
					position: this.supportedTarget
				});
			}else{
				this.preview.out();
			}
		}else{
			this.preview.moveImg({
					pageX : event.pageX,
					pageY : event.pageY,
					position: this.supportedTarget
				});
		}
	},
	out: function(){
		this.preview.out();
	},
	isFixedTrap: function() {
		return !Const.TRAPS[this._trapId][2];
	},
	getTrapId: function() {
		return this._trapId;
	},
	_select: function() {
		this._levelController.selectedIcon = this;
	}
});

c.c('Trap',{
	init: function(){
		this.requires("2D, Image, Harmful," + Const.RENDER);
	},
	trap: function(e){
		switch(e.position)
		{
			case "Top":
				this.attr({x:e.x - this.w / 2, y:e.y - this.h});
				break;
			case "Left":
				this.attr({x:e.x - this.w, y:e.y - this.h / 2});
				break;
			case "Right":
				this.attr({x:e.x + e.w, y:e.y - this.h / 2});
				break;
			case "Bot":
				this.attr({x:e.x - this.w / 2, y:e.y + this.h});
				break;
			default:
				break;
		}
	}
});
c.c('TrapHint',{
	init: function(){
		this.requires("2D, Image, Tween, " + Const.RENDER);
	},
	trapHint: function(e){
		switch(e.position)
		{
			case "Top":
				this.attr({x:e.x - this.w / 2, y:e.y - this.h});
				break;
			case "Left":
				this.attr({x:e.x - this.w, y:e.y - this.h / 2});
				break;
			case "Right":
				this.attr({x:e.x + e.w, y:e.y - this.h / 2});
				break;
			case "Bot":
				this.attr({x:e.x - this.w / 2, y:e.y + this.h});
				break;
			default:
				break;
		}		
		this.tween({disappear: 1}, Const.GAME.TRAP_HINT - 500);
		this.bind('TweenEnd ', this._hintTweenEnd);
	},
	_hintTweenEnd: function(e) {
		if (e == 'disappear') {
			this.tween({alpha: 0.0, disappeared: 1}, 500);
		}
		if (e == 'disappeared') {
			this.destroy();
		}
	}
});

c.c('TrapPreview',{
		init: function(){
			this.requires("2D,Image,"+Const.RENDER);
			this.activate = false;
			this.attr({x:-200,y:200,z:120});
			this.alpha = 0.5;
			return this;
		},
		over: function(elem){
			this.position = elem.position;		
			switch(this.position)
			{
				case "Top":
					this.valueX = elem.layerX - elem.pageX + c.viewport.x - this.w/2;
					this.y  = elem.y - this.h;
					
					break;
				case "Left":
					this.x = elem.x - this.w ;
					this.valueY = elem.layerY - elem.pageY - this.h/2;
					break;
				case "Right":
					this.x = elem.x + this.w ;
					this.valueY = elem.layerY - elem.pageY - this.h/2;
					break;
				case "Bot":
					this.valueX = elem.layerX - elem.pageX + c.viewport.x - this.w/2;
					this.y  = elem.y + this.h;
					break;
				default:
					break;
			}
		},
		out: function(elem){
			this.x = -200;
			this.position = null;
		},
		moveImg: function(elem){
			switch(this.position)
			{
				case "Top":
					this.x = this.valueX + elem.pageX - c.viewport.x;
					break;
				case "Left":
					this.y = this.valueY + elem.pageY;
					break;
				case "Right":
					this.y = this.valueY + elem.pageY;
					break;
				case "Bot":
					this.x = this.valueX + elem.pageX - c.viewport.x;
					break;
				default:
					break;
			}
		}
	});
	
	c.c('Piege1Icon', {
		init: function() {
			this.requires('TrapIcon');
			this.image(Const.ASSETS.PIEGE_1_MINI);
			this.preview = c.e('TrapPreview').image(Const.ASSETS.PIEGE_1);
		}
	});
	c.c('Piege1', {
		init: function() {
			this.requires('Trap, Collision');
			this.image(Const.ASSETS.PIEGE_1);
			this.collision([12,0],[10,20],[23,20],[23,0]);
		}
	});

	c.c('Piege2Icon', {
		init: function() {
			this.requires('TrapIcon');
			this.image(Const.ASSETS.PIEGE_2_MINI);
			this.preview = c.e('TrapPreview').image(Const.ASSETS.PIEGE_2);
		}
	});
	c.c('Piege2', {
		init: function() {
			this.requires('Trap');
			this.image(Const.ASSETS.PIEGE_2);
			this.bind('EnterFrame', this._enterFramePiege2);
		},
		_enterFramePiege2: function() {
			this.x -= 7; 
		}
	});
	c.c('Piege2Hint', {
		init: function() {
			this.requires('TrapHint');
			this.image(Const.ASSETS.PIEGE_2);
			this.alpha = 0.4;
		}
	});

	c.c('Piege3Icon', {
		init: function() {
			this.requires('TrapIcon');
			this.image(Const.ASSETS.PIEGE_3_MINI);
			this.preview = c.e('TrapPreview').image(Const.ASSETS.PIEGE_3);
		}
	});
	c.c('Piege3', {
		init: function() {
			this.requires('Trap');
			this.image(Const.ASSETS.PIEGE_3);
			this.bind('EnterFrame', this._enterFramePiege3); 
		},
		_enterFramePiege3: function() {
			this.x += 7; 
		}
	});
	c.c('Piege3Hint', {
		init: function() {
			this.requires('TrapHint');
			this.image(Const.ASSETS.PIEGE_3);
			this.alpha = 0.4;
		}
	});
	
	c.c('Piege4Icon', {
		init: function() {
			this.requires('TrapIcon');
			this.image(Const.ASSETS.PIEGE_4_MINI);
			this.preview = c.e('TrapPreview').image(Const.ASSETS.PIEGE_4);
		}
	});
	c.c('Piege4', {
		init: function() {
			this.requires('Trap,Collision');
			this.speed = [0,Const.GAME.GRAVITY];
			this.falling = true;
			this.image(Const.ASSETS.PIEGE_4);
			this.collision();
			this.bind('EnterFrame', this._enterFramePiege4);
			return this;
		},
		_enterFramePiege4: function() {
			var hit = this.hit('Obstacle');
			if (hit) {
				_.each(hit, function(obstacle) {
					if (obstacle.normal.y != 0) {
						if (obstacle.normal.y == -1) {
							this.removeComponent('Harmful');
							if(!obstacle.obj.has("Hero")){
								this.speed[1] = Math.min(this.speed[1], 0);
								this.falling = false;
								this.unbind('EnterFrame');
							}
                            if(!gameState.villainMode){
                                this.addComponent('Obstacle');
                            }
							
						}
					}
				},this);
			}
			
			this.x += this.speed[0];
			this.y += this.speed[1];
			
			if (this.falling) {
				this.speed[1] += Const.GAME.GRAVITY;
			}
		}
	});
	c.c('Piege4Hint', {
		init: function() {
			this.requires('TrapHint');
			this.image(Const.ASSETS.PIEGE_4);
			this.alpha = 0.4;
		}
	});

});
