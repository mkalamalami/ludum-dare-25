define(['constant'], function(Const) {

var Utils = {
	move: function(hero, index, time) {
		var i = 0;
		var data = [false,false,false];
		data[index] = true;
		while (i < time) {
      i += 1000. / c.timer.FPS();
			hero._playerReplayData.push(data.slice());
		}
		hero._playerReplayData.push([false,false,false]);
	},
	// Ne fonctionne pas :(
	hit_hero: function(hero){
		if(hero._spriteDirection == 1)
			hero.animate('Hit-Right', Const.SPRITE_RATE, 40);
		else
			hero.animate('Hit-Left', Const.SPRITE_RATE, 40);
	},
	resetViewport: function() {
		c.viewport.x = c.viewport.y = 0;
		// Fixed items
		var fixedItems = c('FixedItem');
		for (var i = 0; i < fixedItems.length; i++) {
			var fixedItem = c(fixedItems[i]);
			fixedItem.x = fixedItem.screenX - c.viewport.x;
		}
	},
	playSound:  function(sound) {
		if (!soundManager.muted) {
			soundManager.play(sound.ID);
		}
	},
	playMusic: function(sound) {
		if (soundManager.getSoundById(sound.ID).playState == 0) {
			soundManager.stopAll();
			if (!soundManager.muted && soundManager.getSoundById(sound.ID).playState == 0) {
				soundManager.play(sound.ID);
			}
		}
	},
	stopAllSound: function() {
		soundManager.stopAll();
	},
	isPlaying: function(sound) {
		return soundManager.getSoundById(sound.ID).playState != 0;
	}
};

c.c('FixedItem',{ 
	init: function(){
		this.requires('2D');
	},
	fixedItem: function(x, y) {
		this.screenX = this.x = x;
		this.y = y;
		// (pos maintained by level_controller)
		return this;
	},
	setX: function(x) {
		this.screenX = x;
	}
});

c.c('Mask', {
	init: function() {
		this.requires('2D, DOM, Color, Tween');
		this.alpha = 0.0;
		this.color('black');
		this.attr({w: Const.WIDTH, h: Const.HEIGHT});
	},
	mask: function(show) {
		if (show) {
			this.alpha = 1.0;
		}
		this.bind('TweenEnd', this._tweenEnd);
		return this;
	},
	triggerMask: function(duration, callback) {
		this.tween({alpha: - this.alpha + 1.0}, duration || 300);
		this._callback = callback;
		return this;
	},
	_tweenEnd: function() {
		if (this.alpha == 0.0) {
			this.destroy();
		}
		if (this._callback) {
			this._callback();
		}
	}
});

c.c('Cinema', {
	BAND_SIZE: 80,
	TWEEN_DURATION: 500,
	init: function() {
		this.requires('2D, DOM, Color, Tween, FixedItem')
			.color('black')
			.fixedItem(0, -this.BAND_SIZE)
			.attr({w: Const.WIDTH, h: this.BAND_SIZE});
		this._bottom = c.e('2D, DOM, Color, Tween, FixedItem')
			.color('black')
			.fixedItem(0, Const.HEIGHT)
			.attr({w: Const.WIDTH, h: this.BAND_SIZE});
	},
	startCinema: function(noTween) {
		if (noTween) {
			this.y = 0;
			this._bottom.y = Const.HEIGHT - this.BAND_SIZE;
		}
		else {
			this.tween({y: 0}, this.TWEEN_DURATION);
			this._bottom.tween({y: Const.HEIGHT - this.BAND_SIZE}, this.TWEEN_DURATION);
		}
		return this;
	},
	endCinema: function() {
		this.tween({y: c.viewport.y - this.BAND_SIZE}, this.TWEEN_DURATION);
		this._bottom.tween({y: c.viewport.y + Const.HEIGHT}, this.TWEEN_DURATION);
		return this;
	}
});


c.c('ExitToMenu', {
	init: function() {
		this.requires('Keyboard');
		this.bind('KeyDown', function() {
			if (this.isDown('ESC') || this.isDown('BACKSPACE')) {
				c.scene('Menu');
			}
		});
		c.e('2D, DOM, Image, Mouse, FixedItem')
			.attr({z: 1000})
			.fixedItem(10, 530)
			.image(Const.ASSETS.EXIT_BTN)
			.css({'cursor': 'pointer'})
			.bind('Click', function() {
				c.scene('Menu');
			});
	}
});

c.c("FollowHero", {
	init : function(){
		this.requires('2D');
		this.bind("EnterFrame", this._enterFrame);
	},
	followHero: function(hero){
		this.decalage = Const.WIDTH/2;
		this.hero = hero;
		return this;
	},
	_enterFrame: function(){
		c.viewport.x = this.decalage - this.hero._x;
		c.viewport.x = Math.min(0, c.viewport.x);
        
        // Fixed items
        var fixedItems = c('FixedItem');
        for (var i = 0; i < fixedItems.length; i++) {
            var fixedItem = c(fixedItems[i]);
            fixedItem.x = fixedItem.screenX - c.viewport.x;
        }
	}
});

c.c('Subtitle', {
	init: function() {
		this.requires('2D, DOM, Text, Tween, FixedItem')
            .fixedItem(100, 520)
            .attr({w: 800, z: 110})
            .textFont({ type: 'italic' })
            .textColor("#A0A0A0");
	},
	subtitle: function(text, slowness, duration) {
		this._currentIndex = 0;
		this._delayLetter = 0;
		this._duration = duration;
		this._slowness = slowness;
		this._targetText = text;
		this._finished = false;
		this.text('');
		this.bind('EnterFrame', this._enterFrameSubtitle);
		this.bind('TweenEnd ', this._endTween);
	},
	cursed: function(text, slowness, duration) {
		this.subtitle(text, slowness, duration);
	},
	_enterFrameSubtitle: function() {
		if (this._duration > 0) {
      this._duration -= 1000. / c.timer.FPS();
			if (!this._finished) {
				if (--this._delayLetter <= 0) {
					this._currentIndex++;
					this._delayLetter = this._slowness;
					this.text(this._targetText.substr(0, this._currentIndex));
					if (this._targetText.length >= this._currentIndex + 1) {
						this.finished = true;
					}
				}
			}
		}
		else if (this.alpha == 1.0) {
			this.alpha = .99;
			this.tween({alpha: 0.0}, 500);
		}
	},
	_endTween: function() {
		this.destroy();
	}
});
c.c('GoatSubtitle', {
	init: function() {
		this.requires('Subtitle');
		this.attr({x: 100, y: 520, w: 800, z: 110});
		this.textFont({ type: 'italic' }).textColor("#FFEE77");
	}
});
	
c.c('Shake',{ 
	init: function(){
		this.requires('2D, DOM, Tween');
	},
	shake: function(amplitude, duration, ease) {
		this.amplitude = amplitude;
		this.duration = duration;
		this.ease = ease;
		this._shakeX = this.x;
		this._shakeY = this.y;
		// (pos maintained by level_controller)
		return this;
	},
	setX: function(x) {
		this.screenX = x;
	}
});

c.c('MuteButton', {
	init: function() {
		this.requires('2D, DOM, Image, Mouse, Pointer')
			.bind('Click', this._click)
			.update();
	},
	update: function() {
		if (gameState.muted) {
			this.image(Const.ASSETS.SOUND_OFF);
		}
		else {
			this.image(Const.ASSETS.SOUND_ON);
		}
		return this;
	},
	setMusic: function(music) {
		this._music = music;
	},
	_click: function() {
		if (gameState.muted) {
			soundManager.unmute();
			Utils.playMusic(this._music || Const.SOUNDS.MUSIC_INTRO_SHORT);
			this.image(Const.ASSETS.SOUND_ON);
			gameState.muted = false;
		}
		else {
			soundManager.mute();
			this.image(Const.ASSETS.SOUND_OFF);
			gameState.muted = true;
		}
		$.jStorage.set('gameState', gameState); // Save changes
	}
});

c.c('TextButton', {
	init: function() {
		this.requires('2D, DOM, Mouse, Pointer');
		this._label = c.e('2D, DOM, Text, Pointer').attr({w: 300});
		this.attach(this._label);
	},
	textButton: function(label, callback, x, y) {
		this._label.text(label);
		this.bind('Click', callback);
		this.labelOffset(x||0,y||0);
		return this;
	},
	labelOffset: function(x, y) {
		this._label.x += x;
		this._label.y += y;
		return this;
	}
});

c.c('Parallax', {
	init: function() {
		this.bind('EnterFrame', this._parallaxEnterFrame);
		this._viewportOldX = 0;
		this.depth = 3;
	},
	parallax: function(depth) {
		this.depth = depth;
	},
	_parallaxEnterFrame: function() {
		this.x -= (c.viewport.x - this._viewportOldX) / this.depth;
		this._viewportOldX = c.viewport.x;
		if (this.x + c.viewport.x < - Const.WIDTH) {
			this.x += Const.WIDTH*2;
		}
		if (this.x + c.viewport.x > Const.WIDTH) {
			this.x -= Const.WIDTH*2;
		}
	}
});

c.c('HelpIcon', {
    init: function() {
        this.requires('2D, DOM, Image, Mouse, Pointer, FixedItem')
            .image(Const.ASSETS.HELP_ICON)
            .attr({z: 200})
			.fixedItem(50, 530);
        this.bind('Click', this._help);
    },
    _help: function() {
        c('Hero').destroy();
        c('LevelController').destroy();
        var cinema = c.e('Cinema').startCinema();
        c.e('2D, DOM, Image, Mouse, Pointer').image(Const.ASSETS.HELP)
            .attr({x: 60, y: 40, z: 200})
            .bind('Click', function() {
                if (gameState.villainMode) {
                    c.scene('Villain');
                }
                else {
                    c.scene('Hero');
                }
            });
    }
});
return Utils;

});